#ifndef SYRINGEDRIVER_H
#define SYRINGEDRIVER_H

#include "Arduino.h"

class SyringeDriver
{
public:
  explicit SyringeDriver();

  void dispatch(const String &package);
  bool respondReady() const {return m_respondReady;}
  String respond() const {return m_respond;}
  void reset();

private:
  bool m_respondReady;
  String m_respond;
  void addRespond(const String &respond);
};

#endif // SYRINGEDRIVER_H
