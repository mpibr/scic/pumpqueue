#include "syringedriver.h"

SyringeDriver::SyringeDriver() :
  m_respondReady(false),
  m_respond("")
{
}

void SyringeDriver::dispatch(const String &package)
{
  String address = package.substring(0, 2);
  String command = package.substring(2, 5);
  
  if (command == "VER")
    addRespond("VER1234");
  else if (command == "RUN")
    addRespond("RUNSTART");
  else if (command == "STP")
    addRespond("DONE");
  else if (command == "ADR")
    addRespond(address + "S" + address);
}


void SyringeDriver::addRespond(const String &respond)
{
  m_respondReady = true;
  m_respond = respond;
}


void SyringeDriver::reset()
{
  m_respondReady = false;
  m_respond = "";
}
