// simulate Aladdin Pump

#include "serialdriver.h"
#include "syringedriver.h"

const uint32_t baudRate = 9600;
const uint32_t bufferSize = 64;

auto hSerial = SerialDriver(bufferSize);
auto hSyringe = SyringeDriver();

void reset() {
  hSerial.reset();
  hSyringe.reset();
}

void setup() {
  hSerial.openPort(baudRate);
}

void loop() {

  // poll serial stream to receive
  if (hSerial.pollStream()) {
    hSyringe.dispatch(hSerial.package());
  }

  // poll syringe
  if (hSyringe.respondReady()) {
    hSerial.writeRespond(hSyringe.respond());
    reset();
  }

}
