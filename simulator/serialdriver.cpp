#include "serialdriver.h"

SerialDriver::SerialDriver(uint32_t buffersize) :
  m_isReadyRead(false),
  m_bufferRead(""),
  m_bufferPackage("")
{
  m_bufferRead.reserve(buffersize);
  m_bufferPackage.reserve(buffersize);
}


void SerialDriver::openPort(uint32_t baudrate)
{
  Serial.begin(baudrate);
}


void SerialDriver::writeRespond(const String &respond)
{
  Serial.println(respond);
}


bool SerialDriver::pollStream()
{
    m_isReadyRead = false;
    
    if (Serial.available()) {
      char nextChar = (char)Serial.read();
      m_bufferRead += nextChar;
      
      if ((nextChar == '\n') || (nextChar == '\r')) {
        m_bufferPackage = m_bufferRead;
        m_bufferRead = "";
        m_bufferPackage.trim();
        if (m_bufferPackage.length() > 0)
          m_isReadyRead = true;
      }
    }

    return m_isReadyRead;
}


void SerialDriver::reset()
{
  m_bufferPackage = "";
}
