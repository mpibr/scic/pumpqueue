#ifndef DEVICETABLEWIDGET_H
#define DEVICETABLEWIDGET_H

#include <QObject>
#include <QWidget>
#include <QTableWidget>
#include "devicewidget.h"

class DeviceTableWidget : public QTableWidget
{
    Q_OBJECT

public:
    explicit DeviceTableWidget(QWidget *parent = nullptr);
    void closeAll();

private:
    QVector<DeviceWidget*> m_devices;

public slots:
    void on_deviceAdd();
    void on_deviceRemove();
    void on_respond(const QByteArray &package);

signals:
    void buttonClicked(int uid, int gid);
    void masterTick(int gid, const QTime &timestamp);
    void schedule(const QByteArray &package);
    void respondReady(int uid, const QString &message);
    void deviceDisconnected(int uid);
    void notify(const QString &message, const Qt::GlobalColor &color = Qt::black);
};

#endif // DEVICETABLEWIDGET_H
