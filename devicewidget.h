#ifndef DEVICEWIDGET_H
#define DEVICEWIDGET_H

#include <QObject>
#include <QWidget>
#include <QMap>
#include <QPushButton>
#include <QComboBox>
#include <QTableWidgetItem>
#include <QTimer>
#include <QTimeEdit>
#include <QQueue>
#include <QDebug>

#include "deviceattributes.h"

class DeviceWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DeviceWidget(int uid = 0, QWidget *parent = nullptr);

    QMap<int, QTableWidgetItem*> items();
    QMap<int, QWidget*> widgets();

    //bool isPumping() const {return m_timer->isActive();}
    int user() const {return m_itemUser->data(Qt::DisplayRole).toInt();}
    int group() const {return m_itemGroup->data(Qt::DisplayRole).toInt();}
    qreal rate() const {return m_itemRate->data(Qt::DisplayRole).toReal();}
    qreal volume() const {return m_itemVolume->data(Qt::DisplayRole).toReal();}
    QString rateUnit() const {return m_unitsRate->currentData().toString();}
    QString volumeUnit() const {return m_unitsVolume->currentData().toString();}
    //QString elapsed() const {return m_itemElapsed->data(Qt::DisplayRole).toString();}
    //void setElapsed(const QString &timestamp) {setData(m_itemElapsed, timestamp, Qt::DisplayRole);}


private:
    bool m_isMaster;
    int m_timeStep;
    QPushButton *m_button;
    QTimeEdit *m_timeEdit;
    QComboBox *m_unitsRate;
    QComboBox *m_unitsVolume;
    QTableWidgetItem *m_itemState;
    QTableWidgetItem *m_itemUser;
    QTableWidgetItem *m_itemGroup;
    QTableWidgetItem *m_itemRate;
    QTableWidgetItem *m_itemVolume;
    QTimer *m_timer;
    QQueue<QString> m_commandQueue;

    QTableWidgetItem* newItem(const QVariant &value, const bool &isReadOnly = false, const Qt::GlobalColor &color = Qt::black);
    void setData(QTableWidgetItem *item, const QVariant &value, int role = Qt::DisplayRole, const Qt::GlobalColor &color = Qt::black);
    void validateItem(QTableWidgetItem *item, const QVariant &min, const QVariant &max);

    void request(const QString &command, const QVariant &value = 0, const QString &units = "");
    void notifyMessage(const QString &message, const Qt::GlobalColor &color = Qt::black);
    bool isRespondValid(const QString &respond);
    bool isRespondAlarm(const QString &respond);

    void enable();
    void disable();
    void run();
    void stop();

public slots:
    void on_actionClicked(int uid, int gid);
    void on_masterTick(int gid, const QTime &timestamp);
    void on_itemChanged(QTableWidgetItem *item);
    void on_respondReady(int uid, const QString &respond);
    void on_deviceDisconnected(int uid);

private slots:
    void on_buttonClick();
    void on_comboChanged(const QString &text);
    void on_timerTick();

signals:
    void buttonClicked(int uid, int gid);
    void schedule(const QByteArray &package);
    void masterTick(int gid, const QTime &timestamp);
    void notify(const QString &message, const Qt::GlobalColor &color = Qt::black);
};

#endif // DEVICEWIDGET_H
