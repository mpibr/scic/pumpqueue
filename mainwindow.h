#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "devicetablewidget.h"
#include "serialdriver.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    SerialDriver *m_serial;

public slots:
    void on_availablePorts(const QStringList &ports);

private slots:
    void on_pushButton_connect_clicked(bool checked);
};
#endif // MAINWINDOW_H
