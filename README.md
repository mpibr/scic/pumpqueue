# PumpQueue

user interface to control a queue of automatic pumps via serial communication protocol

## serial communication

multiple pumps can be connected in serial. each pump should have an incremental address starting from 0. Baud rate should be identical (9600, 19200). Commands should be preceeded with their address.

RS-232 Data Frmaes:

* baudrate = {9600, 19200}
* frame = 10 bit (8N1)
* start bit = 1
* data bits = 8
* stop bits = 1
* parity = none


Basic Mode Protocol:

* command syntax: ```<command data> <CR>```
* response syntax: ```<STX> <response data> <ETX>```

Safe Mode Protocol (uses CCITT CRC algorithm over \<transmitted data\>):

* command syntax: ```<STX> <length> <command data> <CRC16> <ETX>```
* response syntax: ```<STX> <length> <response data> <CRC16> <ETX>```



```*ADR``` set network address

```VER``` version query, response: NE<model>V<n>.<n>

```RUN[<phase data>]``` starts the pumping program operation

```STP``` stop pumping program

```RAT[<float> <rate units>]``` set / query pumping rate

* \< rate units \>

	* UM = uL / min
	* MM = mL / min
	* UH = uL / hr
	* MH = mL / hr

```VOL[<float> <volume units>]``` set / query volume

* \< volume units \>
	* UL = uL (microliters)
	* ML = mL (milliliters) 