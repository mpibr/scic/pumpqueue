#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_serial(new SerialDriver(this))
{
    ui->setupUi(this);
    setWindowTitle("PumpQueue");
    connect(ui->toolButton_add, &QToolButton::clicked, ui->widget_table, &DeviceTableWidget::on_deviceAdd);
    connect(ui->toolButton_remove, &QToolButton::clicked, ui->widget_table, &DeviceTableWidget::on_deviceRemove);
    connect(m_serial, &SerialDriver::notify, ui->widget_logs, &LogWidget::on_notify);
    connect(m_serial, &SerialDriver::availablePorts, this, &MainWindow::on_availablePorts);
    connect(ui->widget_table, &DeviceTableWidget::notify, ui->widget_logs, &LogWidget::on_notify);
    ui->pushButton_connect->setEnabled(false);
}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_connect_clicked(bool checked)
{
    QString portName = ui->comboBox_port->currentText();
    int baudRate = ui->comboBox_baudRate->currentText().toInt();
    if (checked) {
        m_serial->open(portName, baudRate);
        if (m_serial->isOpen()) {
            connect(ui->widget_table, &DeviceTableWidget::schedule, m_serial, &SerialDriver::on_schedule);
            connect(m_serial, &SerialDriver::readyPackage, ui->widget_table, &DeviceTableWidget::on_respond);
        }
    }

    else {
        m_serial->close();
        ui->widget_table->closeAll();
        disconnect(ui->widget_table, &DeviceTableWidget::schedule, m_serial, &SerialDriver::on_schedule);
        disconnect(m_serial, &SerialDriver::readyPackage, ui->widget_table, &DeviceTableWidget::on_respond);
    }
}


void MainWindow::on_availablePorts(const QStringList &ports)
{
    QStringList::const_iterator it;
    for (it = ports.constBegin(); it != ports.constEnd(); ++it)
        ui->comboBox_port->addItem(*it);

    if (ui->comboBox_port->count() > 0)
        ui->pushButton_connect->setEnabled(true);
}
