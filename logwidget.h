#ifndef LOGWIDGET_H
#define LOGWIDGET_H

#include <QObject>
#include <QWidget>
#include <QPlainTextEdit>
#include <QScrollBar>

class LogWidget : public QPlainTextEdit
{
    Q_OBJECT

public:
    explicit LogWidget(QWidget *parent = nullptr);

public slots:
    void on_notify(const QString &message, const Qt::GlobalColor &color = Qt::black);
};

Q_DECLARE_METATYPE(Qt::GlobalColor)

#endif // LOGWIDGET_H
