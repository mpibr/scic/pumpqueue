#ifndef SERIALDRIVER_H
#define SERIALDRIVER_H

#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QTimer>
#include <QList>
#include <QDebug>

class SerialDriver : public QObject
{
    Q_OBJECT
public:
    explicit SerialDriver(QObject *parent = nullptr);
    void open(const QString &port, int baudrate);
    void close();
    bool isOpen() const {return m_serialPort->isOpen();}

private:
    QSerialPort *m_serialPort;
    QTimer *m_timerPorts;
    QTimer *m_timerWrite;
    QTimer *m_timerRead;
    bool m_isWriting;
    bool m_isReading;
    int m_timeoutDuration;
    int m_bytesWritten;
    QByteArray m_bufferWrite;
    QByteArray m_bufferRead;
    QList<QByteArray> m_schedular;

    void driverNotify(const QString &note, bool isError = false);
    void writeToBuffer(const QByteArray &package);

    QByteArray readPackage(const int delimiter);


public slots:
    void on_schedule(const QByteArray &package);

private slots:
    void on_bytesWritten(qint64 bytes);
    void on_readyRead();
    void on_error(QSerialPort::SerialPortError error);
    void on_timeout();
    void on_detectAvailablePorts();

signals:
    void notify(const QString &message, const Qt::GlobalColor color = Qt::black);
    void readyPackage(const QByteArray &package);
    void availablePorts(const QStringList &ports);
};

#endif // SERIALDRIVER_H
