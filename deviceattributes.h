#ifndef DEVICEATTRIBUTES_H
#define DEVICEATTRIBUTES_H

#include <QStringList>
#include <QMap>
#include <QRegularExpression>

namespace DeviceAttributes {


    const QStringList header = {"action", "state", "elapsed",
                                "user", "group",
                                "rate [value]", "rate [unit]",
                                "volume [value]", "value [unit]"};

    namespace Index {
        const int action = 0;
        const int state = 1;
        const int elapsed = 2;
        const int user = 3;
        const int group = 4;
        const int valueRate = 5;
        const int unitRate = 6;
        const int valueVolume = 7;
        const int unitVolume = 8;
    }

    namespace Command {
        const QString ping = "ADR";
        const QString run = "RUN";
        const QString stop = "STP";
        const QString rate = "RAT";
        const QString volume = "VOL";
    }

    namespace Units {
        const QString ml = "ML";
        const QString ul = "UL";
        const QString ulmin = "UM";
        const QString mlmin = "MM";
        const QString ulhr = "UH";
        const QString mlhr = "MH";
    }

    namespace Respond {

    const QRegularExpression pattern("^[IWSPTUA][?]?[RSTEO]?[0-9]*[.]?[0-9]*[MU]?[LMH]?$");

    const QMap<QChar, QString> status = {{'I', "infusing"},
                                         {'W', "withdrawing"},
                                         {'S', "pumping program stopped"},
                                         {'P', "pumping program paused"},
                                         {'T', "pause phase"},
                                         {'U', "operational trigger wait"},
                                         {'A', "alarm"}};

    const QMap<QChar, QString> alarm = {{'R', "pump was reset (power was interrupted)"},
                                        {'S', "pump motor stalled"},
                                        {'T', "safe mode communication time out"},
                                        {'E', "pumping program error"},
                                        {'O', "pumping program phase is out of range"}};
    }
}

#endif // DEVICEATTRIBUTES_H
