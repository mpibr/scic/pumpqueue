#include "devicewidget.h"

DeviceWidget::DeviceWidget(int uid, QWidget *parent) : QWidget(parent),
    m_isMaster(false),
    m_timeStep(0),
    m_button(new QPushButton("ping", this)),
    m_timeEdit(new QTimeEdit(this)),
    m_unitsRate(new QComboBox(this)),
    m_unitsVolume(new QComboBox(this)),
    m_itemState(newItem("disconnected", true, Qt::red)),
    m_itemUser(newItem(uid, true)),
    m_itemGroup(newItem(uid)),
    m_itemRate(newItem(0.0)),
    m_itemVolume(newItem(0.0)),
    m_timer(new QTimer(this))
{
    m_unitsRate->addItem("uL/min", DeviceAttributes::Units::ulmin);
    m_unitsRate->addItem("mL/min", DeviceAttributes::Units::mlmin);
    m_unitsRate->addItem("uL/hr", DeviceAttributes::Units::ulhr);
    m_unitsRate->addItem("mL/hr", DeviceAttributes::Units::mlhr);
    m_unitsVolume->addItem("uL", DeviceAttributes::Units::ul);
    m_unitsVolume->addItem("mL", DeviceAttributes::Units::ml);

    connect(m_unitsRate, &QComboBox::currentTextChanged, this, &DeviceWidget::on_comboChanged);
    connect(m_unitsVolume, &QComboBox::currentTextChanged, this, &DeviceWidget::on_comboChanged);
    connect(m_button, &QPushButton::clicked, this, &DeviceWidget::on_buttonClick);

    m_timeEdit->setDisplayFormat("hh:mm:ss");
    m_timeEdit->setTimeRange(QTime::fromString("00:00:00"), QTime::fromString("23:59:59"));

    m_timer->setInterval(1000);
    connect(m_timer, &QTimer::timeout, this, &DeviceWidget::on_timerTick);
}


QMap<int, QTableWidgetItem *> DeviceWidget::items()
{
    QMap<int, QTableWidgetItem*> map_items;
    map_items.insert(DeviceAttributes::Index::state, m_itemState);
    map_items.insert(DeviceAttributes::Index::user, m_itemUser);
    map_items.insert(DeviceAttributes::Index::group, m_itemGroup);
    map_items.insert(DeviceAttributes::Index::valueRate, m_itemRate);
    map_items.insert(DeviceAttributes::Index::valueVolume, m_itemVolume);
    return map_items;
}


QMap<int, QWidget*> DeviceWidget::widgets()
{
    QMap<int, QWidget*> map_widgets;
    map_widgets.insert(DeviceAttributes::Index::action, m_button);
    map_widgets.insert(DeviceAttributes::Index::elapsed, m_timeEdit);
    map_widgets.insert(DeviceAttributes::Index::unitRate, m_unitsRate);
    map_widgets.insert(DeviceAttributes::Index::unitVolume, m_unitsVolume);
    return map_widgets;
}


QTableWidgetItem* DeviceWidget::newItem(const QVariant &value, const bool &isReadOnly, const Qt::GlobalColor &color)
{
    QTableWidgetItem* item = new QTableWidgetItem(Qt::DisplayRole);
    setData(item, value, Qt::UserRole);
    setData(item, value, Qt::DisplayRole, color);
    item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    if (isReadOnly)
        item->setFlags(Qt::ItemIsEnabled);
    return item;
}


void DeviceWidget::setData(QTableWidgetItem *item, const QVariant &value, int role, const Qt::GlobalColor &color)
{
    QWidget *parent = this;
    if (item->tableWidget())
        parent = item->tableWidget();

    parent->blockSignals(true);
    item->setData(role, value);
    item->setForeground(QBrush(color));
    parent->blockSignals(false);
}


void DeviceWidget::validateItem(QTableWidgetItem *item, const QVariant &min, const QVariant &max)
{
    QVariant value_new = item->data(Qt::DisplayRole);
    QVariant value_old = item->data(Qt::UserRole);
    bool validator = false;

    if (value_new.type() == QVariant::Type::Int)
        validator = (min.toInt() <= value_new.toInt()) && (value_new.toInt() <= max.toInt());

    if (value_new.type() == QVariant::Type::Double)
        validator = (min.toDouble() <= value_new.toDouble()) && (value_new.toDouble() <= max.toDouble());

    if (validator)
        setData(item, value_new, Qt::UserRole);
    else
        setData(item, value_old, Qt::DisplayRole);
}


void DeviceWidget::request(const QString &command, const QVariant &value, const QString &units)
{
    m_commandQueue.push_back(command);
    QString package = QString::number(user()).rightJustified(2, '0', true) + command;

    if (!units.isEmpty()) {
        package += QString::number(value.toReal(), 'g', 3).rightJustified(4, '0', true);
        package += units;
    }
    emit schedule(package.toUtf8() + '\n');
}


void DeviceWidget::notifyMessage(const QString &message, const Qt::GlobalColor &color)
{
    QString note = "device" + QString::number(user()).rightJustified(2, '0', true) + "::";
    emit notify(note + message, color);
}

bool DeviceWidget::isRespondValid(const QString &respond)
{
    return respond.contains(DeviceAttributes::Respond::pattern);
}


bool DeviceWidget::isRespondAlarm(const QString &respond)
{
    return respond.startsWith("A?");
}


void DeviceWidget::enable()
{
    m_button->setText("run");
    setData(m_itemState, "ready", Qt::DisplayRole, Qt::green);
    //request(DeviceAttributes::Command::rate);
    //request(DeviceAttributes::Command::volume);
}


void DeviceWidget::disable()
{
    m_commandQueue.clear();
    m_button->setText("ping");
    setData(m_itemState, "disconnected", Qt::DisplayRole, Qt::red);
    if (m_timer->isActive())
        m_timer->stop();
}


void DeviceWidget::run()
{
    m_timeStep = 1;
    if (m_timeEdit->time() > QTime(0, 0))
        m_timeStep = -1;
    m_timer->start();
    m_button->setText("stop");
    setData(m_itemState, "pumping", Qt::DisplayRole, Qt::blue);
    request(DeviceAttributes::Command::run);
}


void DeviceWidget::stop()
{
    m_isMaster = false;
    m_timeStep = 0;
    m_timer->stop();
    m_button->setText("run");
    setData(m_itemState, "ready", Qt::DisplayRole, Qt::green);
    request(DeviceAttributes::Command::stop);
}

void DeviceWidget::on_actionClicked(int uid, int gid)
{
    if (gid != group())
        return;

    if (uid == user())
        m_isMaster = true;
    else
        m_isMaster = false;

    if (m_button->text() == "run")
        run();
    else if (m_button->text() == "stop")
        stop();
    else if (m_button->text() == "ping")
        request(DeviceAttributes::Command::ping);
}


void DeviceWidget::on_itemChanged(QTableWidgetItem *item)
{
    int uid = item->row();
    if (uid != user())
        return;

    if (item == m_itemGroup) {
        validateItem(item, 0, item->tableWidget()->rowCount());
    }
    else if (item == m_itemRate) {
        validateItem(item, 0.0, 100.0);
        request(DeviceAttributes::Command::rate, rate(), rateUnit());
    }
    else if (item == m_itemVolume) {
        validateItem(item, 0.0, 100.0);
        request(DeviceAttributes::Command::volume, volume(), volumeUnit());
    }
}


void DeviceWidget::on_respondReady(int uid, const QString &respond)
{
    if (uid != user())
        return;

    QString command = m_commandQueue.dequeue();

    if (!isRespondValid(respond)) {
        notifyMessage(respond + " is invalid respond at command " + command, Qt::red);
        return;
    }

    if (isRespondAlarm(respond)) {
        QString alarm = DeviceAttributes::Respond::alarm.value(respond.at(2), "unknown alarm");
        notifyMessage(alarm + " at command " + command, Qt::red);
        return;
    }

    QString status = DeviceAttributes::Respond::status.value(respond.at(0), "unknown status");
    if (command == DeviceAttributes::Command::ping) {
        notifyMessage("status is" + status + " ping is confirmed");
        enable();
    }
    else if (command == DeviceAttributes::Command::run) {
        notifyMessage("status is" + status + " run is confirmed");
        run();
    }
    else if (command == DeviceAttributes::Command::stop) {
        notifyMessage("status is" + status + " stop is confirmed");
        stop();
    }
    else if (command == DeviceAttributes::Command::rate) {
        notifyMessage("status is" + status + " rate is updated");
    }
    else if (command == DeviceAttributes::Command::volume) {
        notifyMessage("status is" + status + " volume is updated");
    }
    else {
        notifyMessage("unknown command " + command, Qt::red);
    }

}


void DeviceWidget::on_deviceDisconnected(int uid)
{
    if (uid != user())
        return;
    disable();
}


void DeviceWidget::on_buttonClick()
{
    emit buttonClicked(user(), group());
}


void DeviceWidget::on_comboChanged(const QString &text)
{
    if (text.length() == 2)
        request(DeviceAttributes::Command::volume, volume(), volumeUnit());
    else
        request(DeviceAttributes::Command::rate, rate(), rateUnit());
}


void DeviceWidget::on_timerTick()
{
    QTime timestamp = m_timeEdit->time().addSecs(m_timeStep);
    if (m_isMaster)
        emit masterTick(group(), timestamp);
}


void DeviceWidget::on_masterTick(int gid, const QTime &timestamp)
{
    if (gid != group())
        return;
    m_timeEdit->setTime(timestamp);
    if (timestamp == QTime(0, 0))
        stop();
}
