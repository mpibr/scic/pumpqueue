#include "devicetablewidget.h"

DeviceTableWidget::DeviceTableWidget(QWidget *parent) : QTableWidget(parent)
{
    setColumnCount(DeviceAttributes::header.count());
    setHorizontalHeaderLabels(DeviceAttributes::header);
    setShowGrid(true);
    setSelectionMode(QAbstractItemView::NoSelection);
    on_deviceAdd();
}


void DeviceTableWidget::closeAll()
{
    for (int uid = 0; uid < m_devices.size(); uid++) {
        emit deviceDisconnected(uid);
    }
}


void DeviceTableWidget::on_deviceAdd()
{
    int uid = rowCount();
    insertRow(uid);
    DeviceWidget* device = new DeviceWidget(uid, this);

    // set items
    QMapIterator<int, QTableWidgetItem*> itItem(device->items());
    while (itItem.hasNext()) {
        itItem.next();
        setItem(uid, itItem.key(), itItem.value());
    }

    // set widgets
    QMapIterator<int, QWidget*> itWidget(device->widgets());
    while (itWidget.hasNext()) {
        itWidget.next();
        setCellWidget(uid, itWidget.key(), itWidget.value());
    }

    connect(this, &DeviceTableWidget::itemChanged, device, &DeviceWidget::on_itemChanged);
    connect(this, &DeviceTableWidget::buttonClicked, device, &DeviceWidget::on_actionClicked);
    connect(this, &DeviceTableWidget::masterTick, device, &DeviceWidget::on_masterTick);
    connect(device, &DeviceWidget::buttonClicked, this, &DeviceTableWidget::buttonClicked);
    connect(device, &DeviceWidget::masterTick, this, &DeviceTableWidget::masterTick);
    connect(device, &DeviceWidget::schedule, this, &DeviceTableWidget::schedule);
    connect(device, &DeviceWidget::notify, this, &DeviceTableWidget::notify);
    connect(this, &DeviceTableWidget::respondReady, device, &DeviceWidget::on_respondReady);
    connect(this, &DeviceTableWidget::deviceDisconnected, device, &DeviceWidget::on_deviceDisconnected);
    m_devices.append(device);
}


void DeviceTableWidget::on_deviceRemove()
{
    int uid = rowCount() - 1;
    if (uid > 0) {
        DeviceWidget* device = m_devices.at(uid);
        //if (device->isPumping())
        //    return;
        device->deleteLater();
        m_devices.remove(uid);
        removeRow(uid);
    }
}


void DeviceTableWidget::on_respond(const QByteArray &package)
{
    QString message(package);

    if (message.size() < 3)
        return;

    int address = message.midRef(0, 2).toInt();
    QString command = message.mid(2);
    emit respondReady(address, command);

    //emit notify("respond: " + message + " siz:" + QString::number(message.length()), Qt::red);
}
